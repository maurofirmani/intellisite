import { createContext, useReducer, useContext } from "react";
import client from "../client/client";

const VideoStateContext = createContext();
const VideoDispatchContext = createContext();

function videoReducer(state, action) {
  switch (action.type) {
    case "toogleButton": {
      return { ...state, isPlaying: !state.isPlaying };
    }
    case "toogleVolume": {
      return { ...state, volume: action.volume };
    }
    case "selectVideo": {
      return { ...state, video: action.video };
    }
    case "loadingCategories": {
      return { ...state, loadingCategories: true };
    }
    case "categoriesLoaded": {
      return { ...state, categories: action.categories };
    }
    case "categoriesError": {
      return { ...state, categoriesError: action.categoriesError };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function VideoProvider({ children }) {
  const [state, dispatch] = useReducer(videoReducer, {
    categories: [],
    status: null,
    volume: null,
    video: null,
    isPlaying: false,
  });
  return (
    <VideoStateContext.Provider value={state}>
      <VideoDispatchContext.Provider value={dispatch}>
        {children}
      </VideoDispatchContext.Provider>
    </VideoStateContext.Provider>
  );
}

function useVideoState() {
  const context = useContext(VideoStateContext);
  if (context === undefined) {
    throw new Error("useVideoState must be used within a VideoProvider");
  }
  return context;
}

function useVideoDispatch() {
  const context = useContext(VideoDispatchContext);
  if (context === undefined) {
    throw new Error("useVideoDispatch must be used within a VideoProvider");
  }
  return context;
}

async function loadCategories(dispatch) {
  dispatch({ type: "loadingCategories" });
  try {
    const { data } = await client.get("/");
    dispatch({ type: "categoriesLoaded", categories: data.categories });
  } catch (error) {
    dispatch({ type: "categoriesError", error });
  }
}

export { VideoProvider, useVideoState, useVideoDispatch, loadCategories };
