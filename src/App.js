import { Box } from "@material-ui/core";

import ControlsComponents from "./components/ControlsComponent";
import DisplayComponent from "./components/DisplayComponent";
import {
  VideoProvider,
} from "./context/video.context";

function App() {
  console.log('hola')
  return (
    <Box height="100vh" display="flex">
      <VideoProvider>
        <ControlsComponents />
        <DisplayComponent />
      </VideoProvider>
    </Box>
  );
}

export default App;
