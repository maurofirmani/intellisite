import { createRef, useEffect } from "react";
import { Box, makeStyles } from "@material-ui/core";
import { useVideoState } from "../../context/video.context";

const useStyles = makeStyles((theme) => ({
  videoStyle: {
    width: "60%",
  },
  title: {
    marginBottom: "4px",
  },
  subTitle: {
    marginTop: "0px",
  },
}));

function DisplayComponent() {
  const classes = useStyles();
  const { isPlaying, volume, video } = useVideoState();
  const vidRef = createRef();

  useEffect(() => {
    if (vidRef.current) {
      const a = isPlaying ? "play" : "pause";
      vidRef.current[a]();
    }
  }, [isPlaying, vidRef]);

  useEffect(() => {
    if (vidRef.current) {
      vidRef.current.volume = volume / 100;
    }
  }, [volume, vidRef]);

  return (
    <>
      {video ? (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          width="100%"
          flexDirection="column"
        >
          <h2 className={classes.title}>{video?.title}</h2>
          <h6 className={classes.subTitle}>{video?.subtitle}</h6>
          <video
            className={classes.videoStyle}
            ref={vidRef}
            src={video?.sources[0]}
          />
          <Box width="60%">
            {video && (
              <>
                <h3>
                  <strong>Description: </strong>
                </h3>
                <p>{video.description}</p>
              </>
            )}
          </Box>
        </Box>
      ) : null}
    </>
  );
}

export default DisplayComponent;
