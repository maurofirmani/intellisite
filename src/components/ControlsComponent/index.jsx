import { useState, useEffect } from "react";

import {
  Box,
  IconButton,
  Grid,
  Slider,
  List,
  ListItem,
  ListItemText,
  Divider,
  makeStyles,
  Collapse,
  Typography,
} from "@material-ui/core";
import {
  Pause,
  PlayArrow,
  VolumeDown,
  VolumeUp,
  ExpandLess,
  ExpandMore,
} from "@material-ui/icons";

import {
  useVideoState,
  useVideoDispatch,
  loadCategories,
} from "../../context/video.context";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: "250px",
    backgroundColor: theme.palette.background.paper,
  },
  volume: {
    padding: "20px",
  },
  thumb: {
    width: "100%",
  },
  collapse: {
    overflowY: "scroll",
    maxHeight: "calc(100vh - 185px) !important",
  },
}));

const URL = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/'

function ControlsComponent() {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const dispatch = useVideoDispatch();
  const { categories, isPlaying, volume } = useVideoState();

  useEffect(() => {
    loadCategories(dispatch);
  }, []);

  const handleClick = () => {
    setOpen((prevState) => !prevState);
  };

  const handleOnControlClick = () => {
    dispatch({ type: "toogleButton" });
  };

  const handleVolumeChange = (e, value) => {
    dispatch({ type: "toogleVolume", volume: value });
  };

  const handleVideoChange = (video) => {
    dispatch({ type: "selectVideo", video });
  };

  const random = (max) => {
    return Math.floor(Math.random() * max)
  }

  const randomVideo = () => {
    const randomCategory = random(categories.length);
    const randomVideo = random(categories[randomCategory].videos.length);
    return categories[randomCategory].videos[randomVideo];
  }

  useEffect(() => {
    if(categories.length) {
      handleVideoChange(randomVideo())
    }
  }, [categories])

  return (
    <div className={classes.root}>
      <Box
        display="flex"
        justifyContent="space-around"
        padding="0 20px"
        margin="0 10px"
      >
        <IconButton onClick={handleOnControlClick}>
          {isPlaying ? <Pause /> : <PlayArrow />}
        </IconButton>
      </Box>
      <Grid container spacing={2} className={classes.volume}>
        <Grid item>
          <VolumeDown />
        </Grid>
        <Grid item xs>
          <Slider
            aria-labelledby="continuous-slider"
            value={volume}
            onChange={handleVolumeChange}
          />
        </Grid>
        <Grid item>
          <VolumeUp />
        </Grid>
      </Grid>
      <Divider />
      <List component="nav" aria-label="secondary mailbox folder">
        {categories?.map((category, index) => (
          <Box key={index}>
            <ListItem button onClick={handleClick}>
              <ListItemText primary={category.name} />
              {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse
              in={open}
              timeout="auto"
              unmountOnExit
              className={classes.collapse}
            >
              {category.videos.map((video, index) => (
                <List key={index} component="div" disablePadding>
                  <ListItem
                    onClick={() => handleVideoChange(video)}
                    button
                    className={classes.thumb}
                  >
                    <Box display="flex" flexDirection="column">
                      <Typography>{video.title}</Typography>
                      <img
                        src={URL + video.thumb}
                        alt={video.title}
                        width="100%"
                      />
                    </Box>
                  </ListItem>
                </List>
              ))}
            </Collapse>
          </Box>
        ))}
      </List>
    </div>
  );
}

export default ControlsComponent;
