import axios from "axios";

const client = axios.create({
  baseURL: "https://api.jsonbin.io/b/60340fc4f1be644b0a63433c",
  responseType: "json",
});

export default client;
